#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <time.h>

#include <dbDefs.h>
#include <registryFunction.h>
#include <subRecord.h>
#include <aSubRecord.h>
#include <epicsExport.h>

//Global variable
int Debug;

static long sendsms(aSubRecord *precord)
{
   FILE *fp;
   char num[41]="blank";
   char msg[41]="blank";
   char cmd[256]=" ";
   char resp[256]=" ";
   char *lvala;
   char now[128]=" ";
   char stat[512]=" ";

   int pclose_status=0;

   time_t t = time(NULL);
   struct tm tms = *localtime(&t);

   lvala=precord->vala;

   strcpy(num, precord->a);
   strcpy(msg, precord->b);

   sprintf(cmd, "./gw3/notify-poseidon-sms.pl -H 172.31.180.77 -M \"%s\" -D %s", msg, num);

   sprintf(now, "%d-%02d-%02d %02d:%02d:%02d", tms.tm_year + 1900, tms.tm_mon + 1, tms.tm_mday, tms.tm_hour, tms.tm_min, tms.tm_sec);

   fp = popen(cmd, "r");
   //fp = NULL;

   if(fp == NULL){
	sprintf(resp, "%s: GSM script failed to execute", now);
   }else{
	fgets(resp, sizeof(resp)-1, fp);
   }

   sprintf(stat, "%s %s", now, resp);

   strcpy(lvala, stat);

    if (Debug){
        printf("Record %s called (%p)\n", precord->name, (void*) precord);
	//printf("CMD: sendsms -#%s -msg \"%s\" \n", num, msg);
	printf("%s\n", cmd);
	printf("%s\n", now);
	printf("%s\n", lvala);
	fflush( stdout );
    }

    pclose_status = pclose(fp);
    if(Debug) printf("Pclose returned %d\n", pclose_status);

    return 0;
}

/* Register these symbols for use by IOC code: */
epicsExportAddress(int, Debug);
epicsRegisterFunction(sendsms);

