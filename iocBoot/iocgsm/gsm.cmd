#!../../bin/linux-x86_64/gsm

## You may have to change gsm to something else
## everywhere it appears in this file

< envPaths

cd "${TOP}"

## Register all support components
dbLoadDatabase "dbd/gsm.dbd"
gsm_registerRecordDeviceDriver pdbbase

## Set global variable
var Debug 0

## Load record instances
dbLoadRecords("db/gsm.db","BL=PINK,DEV=GSM")

cd "${TOP}/iocBoot/${IOC}"

## Autosave 
set_savefile_path("/EPICS/autosave")
set_pass0_restoreFile("auto_settings.sav")

iocInit

## Autosave
create_monitor_set("auto_settings.req", 30, "BL=PINK,DEV=GSM")

## Start any sequence programs
#seq sncxxx,"user=epics"
